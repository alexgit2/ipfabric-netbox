---
description: On this page, sources for the IP Fabric NetBox plugin are described and the part they play in ingesting data into NetBox.
---

# Sources

Sources are a key part of the plugin, they are used to define the connection to IP Fabric. Once configured, sources will be used by background tasks to synchronize data from IP Fabric to NetBox. Multiple sources can be configured if multiple IP Fabric instances are being used such as a production and development instance.

## Prerequisites

Before creating a source, you will need to have the following information to hand:

- IP Fabric URL
- IP Fabric API Token

To create an API Token, please see the [IP Fabric Documentation](../../../IP_Fabric_Settings/integration/api_tokens.md).

### Parameters

| Field       | Description                                                               | Type         |
| ----------- | ------------------------------------------------------------------------- | ------------ |
| Name        | Name of the source                                                        | `CharField`  |
| Type        | Local or Remote Collection                                                | `CharField`  |
| URL         | IP Fabric URL                                                             | `CharField`  |
| API Token   | IP Fabric API Token                                                       | `CharField`  |
| Verify      | Validate HTTPS Certificate (use false if using a self-signed certificate) | `Boolean`    |
| Description | Description for the source                                                | `CharField`  |
| Comments    | Provide additional comments for the source                                | `CharField`  |
| Tags        | Apply tags to the source                                                  | `ForeignKey` |

!!! info

    If your IP Fabric instance is using a self-signed certificate, you will need to set the `Verify` field to `false`. This will disable the validation of the certificate. Currently, there is no way to validate custom certificates, we are working on that.

## Creating a Source

To create a source, navigate to **Plugins --> IP Fabric --> Sources --> Add**. This will bring up a form to enter the details of the IP Fabric instance.

If type is set to `Remote`, data for the snapshots will need to be pushed via API. The API endpoint to do this can be seen in the NetBox OpenAPI spec.

![Create Source](../images/user_guide/source_new.png)

## Syncing a Source

Once a source has been added to NetBox, it does not mean that data will be synchronized. NetBox does not know what snapshots to synchronize from IP Fabric. So first the snapshots must be synchronized.

Navigate to your source and click the **Sync** button. This will create a background task to synchronize the snapshots from IP Fabric.

![Sync Source](../images/user_guide/source_sync.png)

Once the synchronization has completed, you will see the number of snapshots shown within the **Related Objects** section.

![Source Snapshots](../images/user_guide/source_snapshots.png)

## Snapshots

Clicking through to the snapshots will show you all the snapshots that have been synchronized from IP Fabric into NetBox. There will be one snapshot created with the `snapshot_id` of `$last`, this is IP Fabric's way of getting information from the API for the latest snapshot.

![Snapshots](../images/user_guide/snapshots.png)

Each snapshot will show the following information:

- IP Fabric Version
- Status
- Site Count
- Sites
- Device Count
- Interface Count
- Note

This information is used when creating an [ingestion](05_ingestion.md) to determine the snapshot and site to synchronize from IP Fabric to NetBox. This page also contains the raw JSON for the snapshot that is returned via the IP Fabric API.

![Snapshot Detail](../images/user_guide/snapshot_detail.png)
