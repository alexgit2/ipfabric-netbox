---
description: Overview of the NetBox plugin, what it contains, and how to use it.
---

# Overview

This plugin allows the integration and data synchronization between IP Fabric and NetBox.

The plugin uses IP Fabric to collect network data utilizing the [IP Fabric Python SDK](https://gitlab.com/ip-fabric/integrations/python-ipfabric). This plugin relies on helpful features in NetBox like [Staged Changes](https://docs.netbox.dev/en/stable/plugins/development/staged-changes/) and [Background Tasks](https://docs.netbox.dev/en/stable/plugins/development/background-tasks) to make the job of bringing in data to NetBox easier.

- Multiple IP Fabric Sources
- Transform Maps
- Scheduled Synchronization
- Diff Visualization

## Screenshots

![Source](images/user_guide/source_sync.png)

![Snapshots](images/user_guide/snapshot_detail.png)

![Transform Maps](images/user_guide/tm_edit_hostname.png)

![Ingesion](images/user_guide/ingestion_detail.png)

![Diff](images/user_guide/branch_changes_update_diff.png)
