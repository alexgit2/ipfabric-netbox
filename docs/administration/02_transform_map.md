---
description: Installation instructions importing transform maps.
---

# Transform Map Installation

By default, we provide users with the option to create their own transform map. This allows users to customize the transform map to their specific needs. However, we also offer a default transform map that can serve as a starting point. You can install these transform maps as an additional step.

## Installation Instructions Restore Method

Within the transform map page, you can easily restore the default transform maps. This will overwrite any existing transform maps. You can do this by selecting restore in the top-right corner of the transform map page.

## Installation Instructions Manual Method

Follow these steps to install the transform maps:

1. Download the script using `wget`:

   ```shell
   wget -O update_tm.sh "https://gitlab.com/ip-fabric/integrations/ipfabric-netbox/-/raw/main/scripts/update_tm.sh"
   ```

2. Make the script executable:

   ```shell
   chmod +x update_tm.sh
   ```

3. Run the script to install the transform maps:

   ```shell
   ./update_tm.sh
   ```

After completing these steps, the transform maps will be installed and ready to use in NetBox.
