---
description: Detailed installation instructions for the IP Fabric NetBox plugin.
---

# Plugin Installation Instructions

## Prerequisites

- Ensure you have NetBox version `3.5.0` or higher installed.

## Installation Steps

1. **Install the plugin**:

   The plugin is available as a Python package on PyPI, and you can install it using `pip`:

   ```bash
   pip install ipfabric_netbox
   ```

2. **Ensure automatic re-installation**:

   To make sure the plugin is automatically reinstalled during future upgrades, follow these steps:

   - Create a file named [local_requirements.txt](https://docs.netbox.dev/en/stable/installation/3-netbox/#optional-requirements) in the NetBox root directory if it doesn't already exist.

   - Inside the [local_requirements.txt](https://docs.netbox.dev/en/stable/installation/3-netbox/#optional-requirements) file, add the `ipfabric_netbox` package.

3. **Enable the plugin**:

   After installing the plugin, you need to enable it in the NetBox configuration file (`configuration.py`).

   - Open `configuration.py` and locate the `PLUGINS` list.

   - Add `'ipfabric_netbox'` to the `PLUGINS` list as shown below:

     ```python
     PLUGINS = ['ipfabric_netbox']
     ```

4. **Run the upgrade script**:

   To apply the changes, run the NetBox upgrade script. You can use the following command:

   ```bash
   sudo /opt/netbox/upgrade.sh
   ```

5. **Restart services (if necessary)**:

   After running the upgrade script, you may need to restart services:

   ```bash
   sudo systemctl restart netbox netbox-rq
   ```

   For more details on restarting services, refer to the [documentation](https://docs.netbox.dev/en/stable/installation/upgrading/#4-run-the-upgrade-script).

These steps will ensure the successful installation and activation of the plugin within your NetBox environment.

## Transform Map Installation

If you would like to install our default transform maps, see the [documentation](02_transform_map.md).
