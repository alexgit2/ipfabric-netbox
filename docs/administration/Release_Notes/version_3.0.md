---
description: v3.0.0 Release Notes
---

# v3.0 Release Notes

## v3.0.2 (2024-05-28)

### Enhancements
- [SD-649](https://ipfabric.atlassian.net/browse/SD-649) - Maintain change logs during merge.

## v3.0.1 (2024-05-23)

### Bug Fixes
- [SD-647](https://ipfabric.atlassian.net/browse/SD-647) - Alter sync template to support NetBox 4.0.1.

## v3.0.0b0 (2024-05-17)

### Enhancements

- [SD-646](https://ipfabric.atlassian.net/browse/SD-646) - Support NetBox 4.0.

## v3.0.0 (2024-05-23

Release GA. No changes since v3.0.0b0.